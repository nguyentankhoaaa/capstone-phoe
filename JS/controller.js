const BASE_URL= `https://63b2c9a15e490925c521a619.mockapi.io/phone`
let listPhone=[];
// render DSDT
export let productList = () =>{
  axios({
    url : BASE_URL,
    method:"GET"
  }).then((res) => {
    let itemList = res.data;
    for(var index = 0;index < itemList.length;index++){
      let item = itemList[index];
      listPhone.push(item)
    }    
  
  })
  .catch((err) => {
    console.log(err);
  });
      return listPhone;
    }
 

export let renderListPhone = ()=>{
var contentListPhone = "";
for(var index = 0;index < listPhone.length;index++){
  let phone = listPhone[index]
let contentPhone = `
<div class="card" style="width: 18rem;">
<img class="card-img-top" src="${phone.img}" alt="Card image cap">
<div class="card-body">
  <h6 class="card-title">${phone.name}</h6>
  <p class="card-text">${phone.desc}</p>
  <p class="card-text price">${phone.price}₫</p>
  <button class="btn btn-danger" onclick="themVaoGio('${phone.id}')">Add to cart </button>
</div>
</div>`
contentListPhone+=contentPhone;
}


return contentListPhone;
}
export let checkPhoneSamsung = ()=>{
  
  let contentSamsung="";
 for(var index = 0;index <listPhone.length;index++){
  let item = listPhone[index];
  if(item.type =="Samsung" ){
let content = `<div class="card" style="width: 18rem;">
<img class="card-img-top" src="${item.img}" alt="Card image cap">
<div class="card-body">
  <h6 class="card-title">${item.name}</h6>
  <p class="card-text">${item.desc}</p>
  <p class="card-text price">${item.price}₫</p>
  <button class="btn btn-danger" onclick="themVaoGio('${item.id}')">Add to cart </button>
</div>
</div>`
contentSamsung+=content;
  }
}
return contentSamsung;

}
export let checkPhoneIphone = ()=>{
  
  let contentIphone="";
 for(var index = 0;index <listPhone.length;index++){
  let item = listPhone[index];
  if(item.type =="iphone" ){
let content = `<div class="card" style="width: 18rem;">
<img class="card-img-top" src="${item.img}" alt="Card image cap">
<div class="card-body">
  <h6 class="card-title">${item.name}</h6>
  <p class="card-text">${item.desc}</p>
  <p class="card-text price">${item.price}₫</p>
  <button class="btn btn-danger" onclick="themVaoGio('${item.id}')">Add to cart </button>
  
</div>
</div>`
contentIphone+=content;
  }
}
return contentIphone;
}

export let renderDSSP =(spArr) =>{
  let contentDSSP = "";
  for(var index = 0;index < spArr.length;index++){
      var item = spArr[index]
      var content =`<tr>
<td> ${item.name}</td>
<td>${item.price}₫</td>
<td><div class="buttons_added">
<input aria-label="quantity" class="input-qty" min="1" max="50" type="number" id="number" value="">
</div>
</td>
<td><button class="btn btn-danger" onclick="xoaSanPham('${item.id}')">Xóa</button></td>

</tr>`
contentDSSP+=content;
  }
document.getElementById("tbodySP").innerHTML=contentDSSP
}

export let kiemTraViTri = ( idArr,spArr)=>{
  var vitri = -1;
  for ( var index = 0;index <spArr.length;index++){
      var item = spArr[index];
    if(idArr==item.id){
      vitri = index;
    }
  }
  return vitri;
}

// USER
export let renderBuyPhone =(spArr) =>{
  let contentDSBP = "";
  for(var index = 0;index < spArr.length;index++){
      var item = spArr[index]
      var content =`<tr>
<td> ${item.id}</td>
<td>${item.name}</td>
<td>${item.price}₫</td>
<td><p>Screen: ${item.screen}</p>
   <p>Back-Camera:  ${item.backCamera}</p>
   <p>Front-Camera: ${item.frontCamera}</p>
</td>
<td>${item.img}</td>
</tr>`
contentDSBP+=content;
  }
document.getElementById("nav-phone-4").innerHTML= contentDSBP;
}





