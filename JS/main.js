

import { kiemTraId } from "../validate/validate.js";
import {   checkPhoneIphone, checkPhoneSamsung, kiemTraViTri, productList,renderDSSP, renderListPhone} from "./controller.js";




let product = productList();
const timeOut = setTimeout(timeout,1000);
function timeout(){
    document.getElementById("nav-phone").innerHTML =renderListPhone();
    
    
    
    let checkListPhone = ()=>{
        let select = document.getElementById("select2").value;
        let checkphone1 = checkPhoneSamsung();
        if(select =="samsung"){
            document.getElementById("nav-phone").innerHTML ="";
            document.getElementById("nav-phone-3").innerHTML="";
            document.getElementById("nav-phone-2").innerHTML = checkphone1;
        }
        let checkphone2 = checkPhoneIphone();
        if(select =="iphone"){
            document.getElementById("nav-phone").innerHTML ="";
            document.getElementById("nav-phone-2").innerHTML ="";
            document.getElementById("nav-phone-3").innerHTML = checkphone2;
        }
        if(select =="LoaiDienThoai"){
            document.getElementById("nav-phone-3").innerHTML="";
            document.getElementById("nav-phone-2").innerHTML="";
            document.getElementById("nav-phone").innerHTML =renderPhone; 
        }
        
    }
  
    let DSSP=[];
  
    var dsspJson = localStorage.getItem("DSSP");
    if(dsspJson!=null){
        DSSP = JSON.parse(dsspJson);
        renderDSSP(DSSP)
    }
    
   
    
    let themVaoGio=(id)=>{
        document.getElementById("tongtien").style.display="block";
        document.getElementById("modal-content").style.display="block"
        document.getElementById("thanhtoan").style.display="none"
        for(var index=0;index <= product.length;index++){
            let item = product[index];
            renderDSSP(DSSP);
          
            if(id==index){
                var isValid = true;
                isValid=kiemTraId(id,DSSP);
                if(isValid){
                    renderDSSP(DSSP);
                    DSSP.push(item);

                   
                    document.getElementById("tbID").innerHTML="";
                    Toastify({
                        text: "Thêm sản phẩm thành công ✓" ,
                        className: "info",
                        style: {
                            background: "linear-gradient(to right, #00b09b, #96c93d)",
                        }
                    }).showToast();
                    var dsspJson = JSON.stringify(DSSP);
                    localStorage.setItem("DSSP",dsspJson);


                  

                    
                }
            }
        }
        
    }

    let xoaSanPham = (idSp) =>{
       var viTri = kiemTraViTri(idSp,DSSP);
     if(viTri != -1){
        DSSP.splice(viTri,1);
        renderDSSP(DSSP);
        document.getElementById("chuThich").innerHTML="";
        document.getElementById("thanhTien").innerHTML="";
        var dsspJson = JSON.stringify(DSSP);
        localStorage.setItem("DSSP",dsspJson);
    }
}
document.getElementById("thanhtoan").style.display="none";
let tinhTongTien = () =>{
    let sum = 0;
    let num = document.getElementById("number").value;
    let content="";
    renderDSSP(DSSP)
    for ( var index = 0;index<DSSP.length;index++){
         let item = DSSP[index];
         let all = num*item.price
         sum+=all;
         let chuThich = ` ${num} ${item.name} `
        content = content  +chuThich + "  ";
    }
    document.getElementById("chuThich").innerHTML=` Sản phẩm bạn chọn : ${content}.`;
        document.getElementById("thanhTien").innerHTML=`Thành tiền : ${sum}₫`
        document.getElementById("thanhtoan").style.display="block"; 
    }

let thanhToan = () =>{
   DSSP.splice(0,DSSP.length);
   renderDSSP(DSSP);
   Toastify({
    text: "Thanh toán thành công ✓" ,
    className: "info",
    style: {
      background: "#007bff",
    }
  }).showToast();
    document.getElementById("chuThich").innerHTML="";
    document.getElementById("thanhTien").innerHTML="";
    document.getElementById("tongtien").style.display="none";
    document.getElementById("modal-content").style.display="none"
    var dsspJson = JSON.stringify(DSSP);
    localStorage.setItem("DSSP",dsspJson);
}


    
window.checkListPhone= checkListPhone;
window.themVaoGio=themVaoGio;
window.xoaSanPham=xoaSanPham;
window.tinhTongTien = tinhTongTien;
window.thanhToan = thanhToan;

}
