const BASE_URL="https://63b2c9a15e490925c521a619.mockapi.io";
function renderDSSP(phones){
  var contentHTML="";
  phones.reverse().forEach(function(item){
    var content =`<tr>
    <td>${item.id}</td>
    <td>${item.name}</td>
    <td>${item.price}</td>
    <td>${item.desc}</td>
    <td>${converseString(30,item.img)}</td>
    <td>
    <button onclick="xoaSanPham('${item.id}')" class="btn btn-danger" >Xóa</button>
    <button onclick="suaSanPham('${item.id}')" class="btn btn-warning" data-toggle="modal"
    data-target="#exampleModalCenter">Sửa</button>
    </td>
    </tr>`
    contentHTML+=content;
  })
  document.getElementById("tbodyPhoneList").innerHTML=contentHTML;
};

function fectchPhoneList(){
  axios({
    url:`${BASE_URL}/phone`,
    method:"GET",
  }).then(function(res){
    var phoneList = res.data;
    console.log(res)
    renderDSSP(phoneList);
  }).catch(function(err){
    console.log(err);
  })
}
fectchPhoneList();

function xoaSanPham(id){
    
  axios({
    url:`${BASE_URL}/phone/${id}`,
    method:"DELETE",
  }).then(function(res){
  fectchPhoneList();
  }).catch(function(err){
   
    console.log(err);
  })
  }
  
  function themSanPham(){
    var dienThoai = layThongTinTuForm();
    axios({
      url:`${BASE_URL}/phone`,
      method:"POST",
      data:dienThoai,
    }).then(function(res){
      fectchPhoneList();
    }).catch(function(err){
      console.log(err);
    })
  }
  function suaSanPham(id){
    batCapNhat();
  axios({
    url:`${BASE_URL}/phone/${id}`,
    method:"GET",
  }).then(function(res){
    document.getElementById("IdSP").disabled=true;
    var phoneItem = res.data;
    showThongTinLenForm(phoneItem);
  }).catch(function(err){
    console.log(err);
  })
  }
  function capNhapPhone(){
    var phone = layThongTinTuForm();
    axios({
      url:`${BASE_URL}/phone/${phone.id}`,
      method:"PUT",
      data:phone,
    }).then(function(res){
  fectchPhoneList();
    }).catch(function(err){
      console.log(err);
    })
  }
  function themMoi(){
    
    document.querySelector(".btn-capNhap").style.display="none";
    document.querySelector(".btn-themSanPham").style.display="block";
  }
